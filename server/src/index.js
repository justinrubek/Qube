import "babel-polyfill";

import app from "./app";
import http from "http";
import socketio from "socket.io";

import config from "./config";

const port = normalizePort(process.env.PORT || config.port);
app.set("port", port);

const server = http.createServer(app);

const io = socketio(server);

let count = 0;

setInterval(() => {
  console.log("updating time");
  io.to("notifications").emit("notification", { title: "Message " + count++, message: "Hello there " });
}, 25000);

io.on("connect", (socket) => {
  // Determine which feeds the client wants to subscribe to
  //
  // *All of them*

  // Join those rooms
  // Notifications and popups should probably be one thing
  socket.join("notifications");
  socket.join("popups");

  console.log("client connected to overlay")

  socket.emit("notification", { title: "Welcome!" , message: "Currently on message: " + count });

  socket.on("disconnect", () => {
    console.log("client disconnected from overlay");
  });

  socket.on("popup", (data) => {
    console.log("Sending popup to overlay");
    io.to("popups").emit("popup", data);
  });

  socket.on("notification", (data) => {
    console.log("Sending notification to overlay");
    io.to("notifications").emit("notification", data);
  })

});

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }
}

function onListening() {
  const addr = server.address();
  console.log(`Listening on port ${addr.port}`);
}
