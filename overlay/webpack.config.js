const webpack = require("webpack");
const path = require("path");

const config = {
  entry: ["babel-polyfill", __dirname + "/src/index.js"],
  cache: false,
  output: {
    path: path.resolve("../public"),
    filename: "bundle.js",
    },
  resolve: {
    extensions: [".js", ".jsx"]
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        exclude: /node_modules/,
        use: "babel-loader"
      },
      {
        test: /\.css$/,
        use: [ 
          { 
            loader: 'style-loader' 
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]_[hash:base64:5]',
              sourceMap: true,
              minimize: true
            }
          }
        ]

      }
    ]},
};

module.exports = config;
