import React from "react";
import PropTypes from "prop-types";

import style from "./style.css";

function copy(item) {
  return JSON.parse(JSON.stringify(item));
}

export default class PropertyForm extends React.Component {
  constructor(props) {
    super(props);
    const fields = this.props.for.properties.map(property => {
      return { name: property.name, value: property.default || "" };
    });

    this.state = { fields };
    this.submit = this.submit.bind(this);
  }

  updateField(name, value) {
    const fields = copy(this.state.fields);

    for (let field of fields) {
      if (field.name == name) {
        field.value = value;
        this.setState({ fields });
        return;
      } else {
        console.log(field.name);
        console.log(name);
      }
    }
  }

  submit(e) {
    const eventName = this.props.for.eventName;
    const data = {};

    for (let field of this.state.fields) {
      data[field.name] = field.value;
    }

    this.props.submit(eventName, data);
    e.preventDefault();
    e.stopPropagation();
  }

  render() {
    const { fields } = this.state;

    const display = fields.map(field => {
      return (
        <div key={field.name} className={style.input_group}>
          <label htmlFor={field.name} className={style.label}>
            {field.name}
          </label>
          <input
            type="text"
            className={style.input}
            id={field.name}
            value={field.value}
            onChange={e => this.updateField(field.name, e.target.value)}
          />
        </div>
      );
    });

    return (
      <div className={style.container}>
        <h2 className={style.title}>{this.props.for.name} </h2>
        <form className={style.form} onSubmit={this.submit}>
          {display}
          <input type="submit" value="Create" />
        </form>
      </div>
    );
  }
}
