import React from "react";
import io from "socket.io-client";

import PropertyForm from "./PropertyForm";
import Popup from "../Overlay/Popup";
import Notification from "../Overlay/Notification";

export default class ControlPanel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.socket = io();

    this.sendEvent = this.sendEvent.bind(this);
  }

  sendEvent(eventName, data) {
    console.log("Sending event: " + eventName);
    console.log("Event data: " + JSON.stringify(data));
    this.socket.emit(eventName, data);
  }

  render() {
    const {} = this.state;

    return (
      <div>
        <PropertyForm for={Popup} submit={this.sendEvent} />
        <PropertyForm for={Notification} submit={this.sendEvent} />
      </div>
    );
  }
}
