import React from "react";
import PropTypes from "prop-types";
import io from "socket.io-client";
import shortid from "shortid";

import NotificationDisplay from "./NotificationDisplay";
import Popup from "./Popup";
import PropertyForm from "./PropertyForm";

function copy(item) {
  return JSON.parse(JSON.stringify(item));
}

const NOTIFICATION_UPDATE_RATE = 5000;
const NOTIFICATION_MIN_LIFE = 5000;

export default class Overlay extends React.Component {
  constructor(props) {
    super(props);
    const socket = io();
    this.socket = socket;

    this.newNotification = this.newNotification.bind(this);
    this.removeNotification = this.removeNotification.bind(this);
    this.createPopup = this.createPopup.bind(this);
    this.removePopup = this.removePopup.bind(this);

    this.state = {
      notifications: [],
      popups: []
    };

    socket.on("notification", data => {
      console.log("New notification: " + JSON.stringify(data));
      this.newNotification(data);
    });

    socket.on("popup", data => {
      this.createPopup(data);
    });

    setInterval(
      () => this.removeNotification(),
      this.props.NOTIFICATION_UPDATE_RATE
    );
  }

  createPopup(data) {
    console.log("Showing popup for " + data.lifetime);
    const popups = copy(this.state.popups);
    const id = shortid.generate();
    popups.push(Object.assign(data, { id }));
    this.setState({ popups });

    // Kill popup when it gets old
    setTimeout(() => {
      this.removePopup(id);
    }, data.lifetime);
  }

  removePopup(id) {
    console.log("Removing popup with id: " + id);
    const copied = copy(this.state.popups);
    console.log("Copied popups");
    console.log(copied);
    const popups = this.state.popups.filter(p => {
      return p.id != id;
    });
    console.log("filtered:");
    console.log(popups);

    this.setState({ popups });
  }
  // Clear the latest notification, called every once in a while
  removeNotification() {
    const notifications = copy(this.state.notifications);

    if (notifications.length > 0) {
      if (
        Date.now() - notifications[0].created >=
        this.props.NOTIFICATION_MIN_LIFE
      ) {
        notifications.shift();
        this.setState({ notifications });
      }
    }
  }

  newNotification(notification) {
    const notifications = copy(this.state.notifications);

    if (notifications.length >= this.props.NOTIFICATION_COUNT) {
      notifications.shift();
    }

    notifications.push(Object.assign(notification, { created: Date.now() }));

    this.setState({ notifications: notifications });
  }

  render() {
    const { notifications, popups } = this.state;

    let popupDisplay = popups.map(popup => {
      return <Popup popup={popup} top={popup.top} left={popup.left} />;
    });

    return (
      <div>
        <PropertyForm>
          <NotificationDisplay notifications={notifications} />
        </PropertyForm>
        {popupDisplay}
        <p>Hello Overlay</p>
      </div>
    );
  }
}

Overlay.defaultProps = {
  NOTIFICATION_UPDATE_RATE: 5000,
  NOTIFICATION_MIN_LIFE: 5000,
  NOTIFICATION_COUNT: 3
};

Overlay.propTypes = {
  NOTIFICATION_UPDATE_RATE: PropTypes.number,
  NOTIFICATION_MIN_LIFE: PropTypes.number,
  NOTIFICATION_COUNT: PropTypes.number
};
