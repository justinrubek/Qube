import React from "react";
import PropTypes from "prop-types";

import style from "./style.css";


export default class PropertyForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        if (this.props.active) {
            let properties = this.props.children.properties;
            return (
                <div className={style.wrapper}>
                    {this.props.children}
                    <div className={style.container}>
                        <h1>Active</h1>
                        <button onClick={() => console.log("Closing not yet implemented")}>Close</button>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div>
                {this.props.children}
                </div>
            );
        }
    }
}