import React from "react";

import Notification from "../Notification";

import style from "./style.css";

export default class NotificationDisplay extends React.Component {
  constructor(props) {
    super(props);

    this.notification_clicked = this.notification_clicked.bind(this);
  }

  notification_clicked(title) {
    console.log("Clicked: " + title);
    // Maybe tell server to remove this notification
  }

  render() {
    // TODO:
    // A. Key for container div
    // B. CSS to display notificationdisplay at an x, y and notifications in it using flex
    let display = this.props.notifications.map((notification, i) => {
      return (
        <Notification
          title={notification.title}
          message={notification.message}
          onNotificationClick={this.notification_clicked}
        />
      );
    });
    return <div className={style.container}>{display}</div>;
  }
}
