import React from "react";
import PropTypes from "prop-types";

import style from "./style.css";

export default class Popup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

        };

    }

    render() {
        if (this.props.popup == null) {
            return <div />;
        }
        return (
        <div className={style.container} style={{ top: this.props.top, left: this.props.left }}>
            <div className={style.text}>{this.props.popup.text}</div>
        </div>
        );
    }

}

Popup.defaultProps = {
    top: "75%",
    left: "80%"
}

Popup.propTypes = {
    top: PropTypes.string,
    left: PropTypes.string
}

Popup.properties = [
    { name: "text", default: "hello there" },
    { name: "lifetime", default: 5000 },
    { name: "top", default: "75%" },
    { name: "left", default: "80%" }
    ];
Popup.eventName = "popup";