import React from "react";

import style from "./style.css";

export default class Notification extends React.Component {
  render() {
    const title = this.props.title;
    const message = this.props.message;

    return (
      <div
        className={style.notification}
        onClick={() => this.props.onNotificationClick(title)}
      >
        <h2 className={style.title}>{title}</h2>
        <p className={style.message}>{this.props.message}</p>
      </div>
    );
  }
}

Notification.properties = [
  { name: "title", default: "Notification" },
  { name: "message", default: "hello there" }
];

Notification.eventName = "notification";
