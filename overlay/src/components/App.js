import React from "react";
import { Route, BrowserRouter, Switch } from "react-router-dom";

import Overlay from "./Overlay";
import ControlPanel from "./ControlPanel";

export default class App extends React.Component {
  render() {
    return (
        <BrowserRouter>
          <div>
            <Switch>
              <Route exact path="/" component={Overlay} />
              <Route path="/control" component={ControlPanel} />
            </Switch>
          </div>
        </BrowserRouter>
        );
  }
}
